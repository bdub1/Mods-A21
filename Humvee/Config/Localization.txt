Key,English
vehicleHumveechassis,Humvee Chassis
vehicleHumveechassisDesc,Parts needed to craft the Humvee.
vehicleHumveebody,Humvee Body
vehicleHumveebodyDesc,Parts needed to craft the Humvee.
vehicleHumvee,Humvee
vehicleHumveeplaceable,Humvee
vehicleHumveeplaceableDesc,Military surplus at its finest!
vehicleHumveeTopless,Humvee
vehicleHumveeToplessplaceable,Humvee (Topless)
vehicleHumveeToplessplaceableDesc,Military surplus at its finest!