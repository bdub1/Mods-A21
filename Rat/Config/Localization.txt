Key,English
vehicleRat,Rat
vehicleRatplaceable,Rat
vehicleRatplaceableDesc,A custom Rat bike.
vehicleRat2,Rat
vehicleRat2placeable,Rat Red
vehicleRat2placeableDesc,A custom Rat bike.
vehicleRatChassis,Rat Chassis
vehicleRatChassisDesc,"The beginning of something...awesome!"
vehicleRatbars,Rat Handlebars
vehicleRatbarsDesc,You might need these.